# Kz.layedit
### 在线预览
[码云Gitee Pages](http://knifez.gitee.io/kz.layedit/)
#### 更新日志
- ##### V18.11.25
感谢来自<a href="https://gitee.com/yhl452493373">杨黄林</a>的反馈
1. [合并优化]  图片上传时图片的宽度，高度单位为px，输入时不需输入单位；
2. [合并优化] 修改图片属性时可以初始化获取设置的图片的宽和；
3. [合并新增] 文件、视频上传/删除时增加done回调，参数为服务器返回的数据；新增field 上传时的文件参数字段名
4. [合并优化] 图片、视频地址为空时，确定给出提示，不能为空；
5. [合并修复] 批量上传时，上传失败会删除相应图片；
6. [合并优化] 超链接弹窗高度自适应；
7. [合并优化] 弹窗使用layer自带按钮
8. [新增] 上传图片视频时可在后台判断服务器是否存在该文件，如存在则返回{code:2,msg:"重复提示",data:{src:"重复文件路径"}}; 会弹窗提示是否调用重复文件，如不需要该功能返回状态码请勿使用2，正常 0，错误 1， 重复 2 、、、
9. [新增] 编辑器外置样式和js引用【quote】
- ##### V18.11.24
1. [新增]  文字背景色设置[fontBackColor]
2. [优化] uploadImage/uploadVideo可只设置url，其余均设置可读取默认值。
3. [已知问题] IE模式下插入表情图片视频等功能均不可用，仅支持少许设置。
- ##### V18.11.23
1. [新增] 多图上传功能[images],配置参数同uploadImage，删除回调同 calldel
- ##### V18.11.16
1. [修复] 空编辑器上传视频并删除后编辑器无法操作问题
2. [修复] 插入锚点功能
3. [移除] 字体/字体大小设置
4. [优化] 右键菜单/段落格式展示效果
5. [优化] 插入视频同时插入p标签，并在左右各加一个空格符，以处理video标签无法选中问题。
6. [新增] 图片上传和视频上传文件限制参数 file/filemine/exts --该参数引用自layupload，详细见<a href="https://www.layui.com/doc/modules/upload.html#options" target="_blank">layuplaod基础参数</a>
7. [新增] 右键删除视频图片的回调方法设置 calldel:{url:''},该设置会调用post方法传递图片(imgpath)/视频地址(filepath)
8. [新增] 开发者模式 devmode,默认为false,false时隐藏添加链接的 打开方式和rel属性
9. [新增] 图片右键添加删除功能
10. [新增] 超链接添加页面新增链接 文本字段，打开方式默认为新页面
11. [新增] 图片视频上传时可在后台检测服务器是否存在相同文件，相同可返回服务器文件地址进行调用，前台有提示，返回码为2
12. [已知问题] 粘贴或赋默认值时会过滤script和style标签，内容中存在错误时编辑器不可用，如存在该问题请检查内容是否正确
- ##### v18.11.12
1. 新增图片右键修改功能，可重新上传图片
2. 修复上传视频什么也不选时也能成功添加bug，现在会提示上传视频(感谢<a href="https://gitee.com/herohill">hreohill</a>的反馈)
3. 新增 添加水平线/hr（<i>addhr</i>）功能
4. 插入代码新增自定义参数 codeConfig{hide:true|false,default:"javascript/c#/java..."} 设置hide为true时不显示代码选择框，可依据default设置默认语言格式。不设置codeConfig则为原版

~~5. [已知bug] 字体大小设置目前不可用~~
~~6. [待完善]新增 插入锚点(<i>anchors</i>) 功能，前台展示默认为 $锚点$ ,保存和读取存在问题，暂不推荐使用~~
- ##### v18.10.23
修复取消全屏后样式错误问题（部分情况下依旧会出现高度变矮情况）
- ##### v18.10.9
1. 新增图片右键修改宽高功能
2. 优化右键面板样式，最大化最小化功能优化
- ##### v18.10.8
1. 添加右键触发事件 --居中，居左，居右，删除
2. 回车、居中居左等自动追加p标签
- ##### v18.9.29
1. 添加HTML源码模式
2. 图片插入添加alt属性
3. 新增 视频插入、全屏、字体颜色设置功能
#### 项目介绍
对layui.layedit的拓展，基于layui v2.4.3.
- 增加了HTML源码模式，
- 图片插入功能添加alt属性（layupload），
- 视频插入功能，
- 全屏功能，
- 段落格式，
- 字体颜色设置功能。
- 所有拓展功能菜单按钮图标均引用自layui自带图标
#### 软件架构
软件架构说明
1. HTML源码模式 引用第三方插件ace,优化源码展示样式。
2. 引用ace编辑器仅保留了html源码样式和tomorrow主题，如有需要可自行更换
#### 安装教程
1. index.html下为示例文件，可供查看演示功能
2. 将dist下文件layedit.js替换掉layui/lay/modules/layedit.js
3. 正常调用layedit即可

#### 使用说明
配置信息

```
     layui.use(['layedit','layer','jquery'],function() {
         var $=layui.jquery;
         var layedit = layui.layedit;
 		 layedit.set({
                //暴露layupload参数设置接口 --详细查看layupload参数说明
                uploadImage: {
                    url: '/Attachment/LayUploadFile',
                    field: 'file',//上传时的文件参数字段名
                    accept: 'image',
                    acceptMime: 'image/*',
                    exts: 'jpg|png|gif|bmp|jpeg',
                    size: 1024 * 10,
                    done: function (data) {//文件上传接口返回code为0时的回调
                    }
                }
                , uploadVideo: {
                    url: '/Attachment/LayUploadFile',
                    field: 'file',//上传时的文件参数字段名
                    accept: 'video',
                    acceptMime: 'video/*',
                    exts: 'mp4|flv|avi|rm|rmvb',
                    size: 1024 * 2 * 10,
                    done: function (data) {//文件上传接口返回code为0时的回调
                    }
                }
                //右键删除图片/视频时的回调参数，post到后台删除服务器文件等操作，
                //传递参数：
                //图片： imgpath --图片路径
                //视频： filepath --视频路径 imgpath --封面路径
                , calldel: {
                    url: '/Attachment/DeleteFile',
                    done: function (data) {//data删除文件接口返回返回的数据
                    }
                }
                //开发者模式 --默认为false
                , devmode: true
                //插入代码设置
                , codeConfig: {
                    hide: false,  //是否显示编码语言选择框
                    default: 'javascript' //hide为true时的默认语言格式
                }           
                //新增iframe外置样式和js
                , quote:{
                    style: ['/Content/Layui-KnifeZ/css/layui.css','/others'],
                    js: ['/Content/Layui-KnifeZ/lay/modules/jquery.js']
                }
                 , //fontFomatt:["p","span"]  //自定义段落格式 ，如不填，默认为 ["p", "h1", "h2", "h3", "h4", "h5", "h6", "div"]~~
                 , tool: [
                     'html','code'
 					, 'strong', 'italic', 'underline', 'del', 
					,'addhr' //添加水平线
					,'|', 'fontFomatt','colorpicker' //段落格式，字体颜色
 					, 'face', '|', 'left', 'center', 'right', '|', 'link', 'unlink'
 					, 'image_alt', 'altEdit', 'video' 
					,'anchors' //锚点
                     , '|', 'fullScreen'
                 ]
         });
         var ieditor = layedit.build('layeditDemo');
     })
```
